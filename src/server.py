#!/usr/bin/env python3
import math
import rospy
import actionlib
from sensor_msgs.msg import LaserScan
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped, PoseArray, PolygonStamped
from nav_msgs.msg import Path
from std_msgs.msg import Bool, Empty, String
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from dynamic_reconfigure.client import Client
from move_base_sequence.srv import *
import time
class move_base_sequence():
    def __init__(self):
        rospy.init_node('move_base_sequence')
        self.ros_name = rospy.get_name();
        self.ignoreObstacle = False
        self.detectObstacle = False

        
        #changed this line to get_param every time so that any change in the parameter would be captured without needing to initialize a new object
        #self._repeating  = rospy.get_param("/move_base_sequence/is_repeating",True)
        self.poses_pub   = rospy.Publisher("/move_base_sequence/wayposes",PoseArray,queue_size=10)          
        #self.poly_pub    = rospy.Publisher("/poly_path",PolygonStamped,queue_size=10)          
        self.path_pub    = rospy.Publisher("/move_base_sequence/path",Path,queue_size=10)   
        self.statusNav   = rospy.Publisher("/move_base_sequence/statusNav",String,queue_size=1)    

        # service_get_state = rospy.get_param("~service_get_statservice_set_statee","/move_base_sequence/get_state")
        # service_toggle_state = rospy.get_param("~service_toggle_state","/move_base_sequence/toggle_state")
        # service_set_state = rospy.get_param("~service_set_state","/move_base_sequence/set_state")
        # service_reset = rospy.get_param("~service_reset","/move_base_sequence/reset")

        _ = rospy.Subscriber("/move_base_sequence/wayposes",PoseArray,self.set_poses)          
        _ = rospy.Subscriber("/move_base_sequence/corner_pose",PoseStamped,self.add_pose_from_corner)          
        _ = rospy.Service("/move_base_sequence/get_state", get_state, self.get_state) 
        _ = rospy.Service("/move_base_sequence/toggle_state", toggle_state, self.toggle_state) 
        _ = rospy.Service("/move_base_sequence/set_state", set_state, self.set_state) 
        _ = rospy.Service("/move_base_sequence/reset", reset, self.reset) 
        _ = rospy.Service("/move_base_sequence/get_currentStatus", get_currentStatus, self.get_currentStatus) 
        _ = rospy.Service("/move_base_sequence/activeGoalAgain", activeGoalAgain, self.activeGoalAgain) 
        # Create subscriber to receive data from lidar
        self.f_scan_sub = rospy.Subscriber('/f_scan_rep117', LaserScan, self.scan_callback)
        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        #_ = rospy.Subscriber("/move_base_sequence/state",Bool,self.set_state)#to switch the sending of the sequnce on or off          
        # = rospy.Subscriber("/move_base_sequence/reset",Empty,self.reset)#to reset the whole sequence          
      
        #rospy.set_param("/move_base_sequence/abortion_behaviour","stop")
      
      
        self.__state__ = True #indicates whether the seq. is running or stopped... edited by /move_base_seq_state topic, should be a service!
        self._i = 0 #how many goals in the list are done          
        self._sending = False #indicates whether there is a goal thats being served or not
        self._internal_call = False #this is to prevent infinite loop of calling self.add_pose() when adding poses through pose array
        self._external_call = False
        self.currentStatus = "free" #this is used to store current status of robot

        self.path = Path()
        self.path.header.frame_id = "map"
        self.path.header.stamp = rospy.Time.now()

        #self.poly_path = PolygonStamped()
        #self.poly_path.header.frame_id = "map"
        #self.poly_path.header.stamp = rospy.Time.now()
        self.poses = PoseArray()
        self.poses.header.frame_id = "map"
        self.poses.header.stamp = rospy.Time.now()

        # Create action client and initial action client MoveBaseAction
        move_base_server = rospy.get_param("~move_base_server","move_base")
        self.is_repeating = rospy.get_param("~move_base_sequence/is_repeating",False)
        rospy.loginfo( self.ros_name+ " Connected to move base server: "+  move_base_server)
        self.client = actionlib.SimpleActionClient(move_base_server,MoveBaseAction)
        rospy.loginfo("Waiting for move_base action server...")
        wait = self.client.wait_for_server(rospy.Duration(5.0))

        #Change dynamic config 
        self.tf_prefix = rospy.get_param("~tf_prefix","") 
        node_reconfigure = self.tf_prefix + "/move_base_node" 
        rospy.loginfo( self.ros_name+ " dynamic_config: "+ node_reconfigure) 
        self.clientDynamic = Client(node_reconfigure) 
        # Configure parameters using the client 
        config = {'oscillation_timeout': 30.0} 
        self.clientDynamic.update_configuration(config) 

        if not wait:
            rospy.logerr("Action server not available!")
            #rospy.signal_shutdown("Action server not available!")
            return
        rospy.loginfo("Connected to move base server, moving the base "+ ("one way trip" if self.is_repeating == False else "in a repetitive way"))
        rospy.loginfo( self.ros_name + " Waiting for goals..")
        self.statusNav.publish("Waiting for goals")


    def __del__(self):
        self.reset(resetRequest())
        self.currentStatus = "free"
        print( self.ros_name + " move_base_sequence ended successfully!")

    def set_cmd_vel(self, vel):
        cmd_vel = Twist()
        cmd_vel.linear.x = vel
        cmd_vel.linear.y = 0.0;
        cmd_vel.angular.z = 0.0
        self.cmd_vel_pub.publish(cmd_vel)
    def scan_callback(self, data):
        dis_front = 10
        for x in range(110, 180):
            dis_front = min(dis_front, data.ranges[x]*math.sin(math.pi * x / 360))
        regions = {
            'front':  dis_front
        }
        # print(str(regions['front']))
        if not self.ignoreObstacle: # and self._sending:
            if regions['front'] < 0.2:
                self.set_cmd_vel(0.0)
                self.detectObstacle = True
                self.statusNav.publish("Detect Obstacle")
            elif regions['front'] < 0.4:
                self.set_cmd_vel(0.2)
            # elif regions['front'] < 0.8:
            #     self.set_cmd_vel(0.4)
                rospy.loginfo( self.tf_prefix + " Detect Obstacle")
                #if self._sending:
                #    # self.client.cancel_goal()
                #    self.__state__ = False
        # else:
            #if not self.__state__ :
            #    self.__state__ = True  

    def reset(self,request):
        try:
            self.__state__ = True 
            self._i = 0           
            #self._internal_call = False
            if self._sending: self.client.cancel_goal()
            self.path = Path()
            self.path.header.frame_id = "map"
            self.path.header.stamp = rospy.Time.now()
            #self.poly_path = PolygonStamped()
            #self.poly_path.header.frame_id = "map"
            #self.poly_path.header.stamp = rospy.Time.now()
            self.poses = PoseArray()
            self.poses.header.frame_id = "map"
            self.poses.header.stamp = rospy.Time.now()

            self._internal_call = False
            self._external_call = False
            self.poses_pub.publish(self.poses)
            #self.poly_pub.publish(self.poly_path)
            self.path_pub.publish(self.path)

            rospy.loginfo( self.ros_name + " Sequence reset done!.. waiting for new goals..")
            return resetResponse(True)
        except:
            #this means a problem happened, may be imporved to add a warning or a log, or even handle the possible errors
            rospy.logerr("A problem occured during resetting!")
            return resetResponse(False)

    def set_state(self,request):
        if self.__state__ == request.state : 
           rospy.logwarn("the sent state is the same as the internal state! no change is applied.")
           return set_stateResponse(False)            
        self.__state__ = not self.__state__
        rospy.loginfo("set_state Success! State now is "+ ("operating" if self.__state__ else "paused."))
        if self.__state__ == False:
            if self._sending: self.client.cancel_goal()
        return set_stateResponse(True)            

    def toggle_state(self,request):
        try:
            self.__state__ = not self.__state__
            if self.__state__ == False:
                if self._sending: self.client.cancel_goal()
            rospy.loginfo("toggle_state Success! State now is "+ ("operating" if self.__state__ else "paused."))
            return toggle_stateResponse(True)            
        except:
            rospy.logerr("Something went wrong while toggling the state!")
            return toggle_stateResponse(False)            


    def get_state(self,request):
         return get_stateResponse("operating" if self.__state__ else "Paused") 
    
    def get_currentStatus(self,request):
         return get_currentStatusResponse(self.currentStatus) 
    
    def activeGoalAgain(self, request):
        if request.activeGoalAgain:
            if self._sending: 
                self.client.cancel_goal()
        else:
             self.ignoreObstacle = False
        return activeGoalAgainResponse(self.ignoreObstacle, self._i) 

    def add_pose(self,msg):
            self._external_call = False
            self.poses.poses.append(msg.pose)
            #self.poly_path.polygon.points.append(self.poses.poses[-1].position)       
            self.path.poses.append(msg)
            self.poses.header.stamp = rospy.Time.now()
            #self.poly_path.header.stamp = rospy.Time.now()
            self.path.header.stamp = rospy.Time.now()
            if self._internal_call: rospy.loginfo("Addinggi a goal from a pose array...")
            else: rospy.loginfo("Adding a new pose to the list.")
            self._external_call = False
            #updating new visualization topics only if the call was a callback for the topic /corner_pose
            #if not self._internal_call:
            #    self._internal_call = True
            #   self.poses_pub.publish(self.poses)
            #    #self.poly_pub.publish(self.poly_path)
            #    self.path_pub.publish(self.path)
    def add_pose_from_corner(self,msg):
            self.poses.poses.append(msg.pose)
            #self.poly_path.polygon.points.append(self.poses.poses[-1].position)       
            self.path.poses.append(msg)
            self.poses.header.stamp = rospy.Time.now()
            #self.poly_path.header.stamp = rospy.Time.now()
            self.path.header.stamp = rospy.Time.now()
            rospy.loginfo("Adding a new pose array to the list.")
            #updating new visualization topics only if the call was a callback for the topic /corner_pose
            self._external_call = True;
           
    def set_poses(self,msg):
        rospy.loginfo("Adding a new pose array " + str(len(msg.poses)) + " goals")

        if len(msg.poses) > 0: 
            if not self._internal_call:
                self._internal_call = True
                temp_posestamped = PoseStamped()
                temp_posestamped.header = self.poses.header
                for pose in msg.poses:
                    temp_posestamped.pose = pose
                    self.add_pose(temp_posestamped)
                self.poses_pub.publish(self.poses)
                #self.poly_pub.publish(self.poly_path)
                self.path_pub.publish(self.path)
                rospy.loginfo("Successfully added goal poses!")
        else: 
            rospy.loginfo("pose array is empty")
            return

    def check_newgoals(self):
        if not self.__state__: 
             return  #state is off, do not send goals!, it won't get here if __state__ is false but as a safety
        elif self._i <= len(self.poses.poses)-1:
          self.move_base_client()
          return
        else:  return  # cool, no new goals :)

        
    def active_cb(self):
        self.currentStatus = "bussy_"+str(self._i)
        if self.detectObstacle:
            self.statusNav.publish("navigate to_"+ str(self._i) + "_again")
            # self.ignoreObstacle = False
            self.detectObstacle = False
        else:
            self.statusNav.publish("navigate to_"+ str(self._i))
        rospy.loginfo( self.ros_name + " Goal pose "+str(self._i)+" is now being processed by the Action Server...")


    def feedback_cb(self, feedback):
        #To print current pose at each feedback:
        #rospy.loginfo("Feedback for goal "+str(self.goal_cnt)+": "+str(feedback))
        pass

 
    def done_cb(self, status, result):
        # Reference for terminal status values: http://docs.ros.org/diamondback/api/actionlib_msgs/html/msg/GoalStatus.html
        if status == 2:
            rospy.loginfo("Goal pose "+str(self._i)+" received a cancel request after it started executing, successfully cancelled!")
            self.__state__ = True
            self.ignoreObstacle = True
            self.currentStatus = "free" #this is used to store current status of robot
        elif status == 8:
            rospy.loginfo("Goal pose "+str(self._i)+" received a cancel request before it started executing, successfully cancelled!")
            self.currentStatus = "free" #this is used to store current status of robot
        elif status == 3:
            rospy.loginfo(self.ros_name+ " Goal pose "+str(self._i)+" REACHED!") 
            self.statusNav.publish("Goal done_"+ str(self._i))
            # self.__state__ = False;
            self._set_next_goal()

        elif status == 4:
            behav = rospy.get_param("~move_base_sequence/abortion_behaviour","continue")
            self.statusNav.publish("Goal done_"+ str(self._i)+"_abort")
            if behav == "stop": self.__state__ = False
            elif not (behav == "continue"): rospy.logwarn("Param /move_base_sequence/abortion_behaviour  is neither 'stop' nor 'continue'! continue is assumed.")
            rospy.logerr ("Goal pose "+str(self._i)+" aborted," +("stopping sequence execution," if behav=='stop' else "continuing with next goals, ")+ "check any errors!")
            self._set_next_goal()

        elif status == 5:
            rospy.logerr("Goal pose "+str(self._i)+" has been rejected by the Action Server. moving to next goal.")
            self._set_next_goal()

        self._sending = False #ended dealing with the goal


    def _set_next_goal(self):
        self._i+=1
        if self._i <= len(self.poses.poses)-1: pass
        elif self._i > len(self.poses.poses)-1:
            if self.is_repeating:
               self._i = 0
               rospy.loginfo("reached the end of the sequence successfully, repeating it again!")
            else:
                rospy.loginfo("_internal_call!" + str(self._internal_call))
                if self._external_call: 
                    rospy.loginfo( self.ros_name+ " reached the last targetPoint, waiting another targetPoint!")
                else:
                    self._internal_call = False
                    self._external_call = False
                    self._i = 0
                    rospy.loginfo( self.ros_name+ " reached the end of the sequence successfully, waiting another sequence!")
                    self.currentStatus = "done"
                    self.__state__ = True
                
                    #clearing the poses seq., path, and visualisation topics   
                    self.path = Path()
                    self.path.header.frame_id = "map"
                    self.path.header.stamp = rospy.Time.now()
                    #self.poly_path = PolygonStamped()
                    #self.poly_path.header.frame_id = "map"
                    #self.poly_path.header.stamp = rospy.Time.now()
                    self.poses = PoseArray()
                    self.poses.header.frame_id = "map"
                    self.poses.header.stamp = rospy.Time.now()
                    self.poses_pub.publish(self.poses)
                    #self.poly_pub.publish(self.poly_path)
                    self.path_pub.publish(self.path)
                    self.statusNav.publish("navigation finish")

    
    def move_base_client(self):
        if  self._sending or not self.__state__: return
        else:
          self._sending = True #don't send as it has already sent a goal thats being served
          goal = MoveBaseGoal()
          goal.target_pose.header.frame_id = "map"
          goal.target_pose.header.stamp = rospy.Time.now() 
          goal.target_pose.pose = self.poses.poses[self._i]
          rospy.loginfo(self.ros_name+ " Sending goal pose "+str(self._i)+" to Action Server")
          #rospy.loginfo(str(self.poses.poses[self._i]))  
          self.client.send_goal(goal, self.done_cb, self.active_cb, self.feedback_cb)



if __name__ == '__main__':
    move_base_sequence = move_base_sequence()
    while not rospy.is_shutdown():
        rospy.sleep(1)
        move_base_sequence.check_newgoals()
        continue
